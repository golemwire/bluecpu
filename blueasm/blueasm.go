package main

import (
	"fmt"
	"os"
	"io"
	
	"gitlab.com/golemwire/blue/asm"
)

func main() {
	var (
		disassemble = false
		
		inputStream io.Reader
		outputStream io.Writer
		outputBuf []byte
		err error
	)
	
	// Select the streams to use:
	if os.Args[0] == "bluedisasm" { // Disassemble
		disassemble = true
		
		switch len(os.Args) {
		case 1:
			inputStream = os.Stdin
			outputStream = os.Stdout
		case 2:
			inputStream, err = os.Open(os.Args[1])
			if err != nil { panic(err) }
			outputStream = os.Stdout
		case 3:
			inputStream, err = os.Open(os.Args[1])
			if err != nil { panic(err) }
			outputStream, err = os.Create(os.Args[2])
			if err != nil { panic(err) }
		default:
			fmt.Fprintln(os.Stderr, "Problem: wrong amount of arguments.")
		}
	} else { // Assemble
		switch len(os.Args) {
		case 1:
			inputStream = os.Stdin
			outputStream = os.Stdout
		case 2:
			inputStream, err = os.Open(os.Args[1] + ".ba")
			if err != nil { panic(err) }
			outputStream, err = os.Create(os.Args[1] + ".bxe")
			if err != nil { panic(err) }
		case 3:
			inputStream, err = os.Open(os.Args[1])
			if err != nil { panic(err) }
			outputStream, err = os.Create(os.Args[2])
			if err != nil { panic(err) }
		default:
			fmt.Fprintln(os.Stderr, "Problem: wrong amount of arguments.")
		}
	}
	
	// Assemble or disassemble:
	if disassemble {
		asm.Disassemble(inputStream, &outputBuf)
		
		// Append newline:
		outputBuf = append(outputBuf, '\n')
	} else {
		successful := asm.Assemble(inputStream, &outputBuf)
		
		if !successful {
			fmt.Fprintln(os.Stderr, "Error: failed to assemble.")
			os.Exit(1)
		}
		
		fmt.Fprintf(os.Stderr, "Assembled in %d (0x%[1]X) bytes.\n", len(outputBuf))
	}
	
	// Write output:
	var n = 0
	for i := 0; i < len(outputBuf); i += n {
		n, err = outputStream.Write(outputBuf[i:])
		if err != nil { panic(err) }
	}
}
