\[Work In Progress\]

# Blue

*“What is the most amount of functionality I can get from the least amount of computer?”*

Blue is a computing stack designed to be as small and relatively painless to assembly program as possible, while being capable and efficient. Blue is designed to be fairly easy to implement and have a small footprint, whether it is implemented as a VM/emulator or maybe even as an actual silicon chip.

BlueCPU is 24-bit. It has 27 instructions. With the Blueroot device/extension, it supports running "processes" \[Processes are still in development.\]

One of the main goals of Blue is to provide a way to create and run programs that cannot go obsolete at the technical level, programs which can run "forever" without requiring "updates" or "support." As long as you can implement (or port) a Blue emulator, you can run past and present Blue binaries forever.  
Presently, BlueCPU is not "feature-frozen" and is still open to upgrades; it therefore does not have the quality of versionless immutability yet.

## Blue Computers, Emulators or Applications

The standard set of devices for a Blue computer, emulator or application describe a simple, stable, PC-like interface called *BluePC*. It has textual I/O (a console), a keyboard, a pointing device (single-button mouse / touchscreen etc.), a 60Hz 4bpc 512×320 RGB screen, and mono audio. A Blue computer or emulator runs at at least 2^24 Hz (~16.8MHz).  
Currently, only textual I/O is supported (Consoledev). However, the Hubdev device has been implemented, as well as some barebones functionality of Blueroot. See the documentation in *Devices.txt*.

## Downloading and Installing the Repository and Toolchain

To get the toolchain and the examples, download the repository:  
\$ `git clone https://gitlab.com/golemwire/bluecpu`  
Then run  
\$ `go install`  
in the blueemu and blueasm folders. The `blueemu` emulator and `blueasm` assembler will then be built and installed to your ~/go/bin folder.  
Symlink "bluedisasm" to `blueasm` for the disassembler:  
\$ `ln -s ~/go/bin/blue{,dis}asm`  
Ensure that ~/go/bin is in your \$PATH (though if you have a thorough Go installation, you should have this done already). To put it on your path temporarily, you may run `PATH="$PATH:$HOME/go/bin"`.

## The Toolchain

### Assembling and Running Blue Programs
To assemble and run a Blue program with the small toolchain here, use the commands `blueasm` and `blueemu`:  
\$ `blueasm hello` # Assemble hello.ba to hello.bxe  
\$ `blueemu hello.bxe` # Execute hello.bxe  
In the folder where you assemble an executable, the lib/ folder should be present, as the assembly code of some programs uses code in it. Consider making a symlink.

### Disassembling and Debugging Blue Programs
Minimal debugging tools are available, via `bluedisasm` and `blueemu <FILE> --trace`.  
On normal Unix terminals you can use Ctrl+S to pause output and Ctrl+Q to resume output. Sending the emulator the SIGUSR1 signal dumps the channels, the registers, and the first 4 words of the stack frame to stderr.  
Note that `bluedisasm` is just `blueasm` called under a different name.

## What is Permacomputing?

Given the limited and manual nature of Blue, it lends itself well to permacomputing.

Definitions of permacomputing may vary, but here is one. Permacomputing is the means and methodology of using hardware and software in a way that isn't wasteful of resources (memory and cycles, yes, but electricity and silicon especially).

BlueCPU is inspired by the [UXN project](https://100r.co/site/uxn.html).