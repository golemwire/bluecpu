package blue

// Blueroot device. Provides some low-level functions such as shutting down.
// Only listens if the low byte of b is 0x00 (as .Dev is probably a hub).
type Bluerootdev struct {
	Dev Device
	
	Stm []byte
	Reg *[4]UInt24
}

func (brd *Bluerootdev)Init(stm []byte, reg *[4]UInt24) {
	brd.Stm = stm
	brd.Reg = reg
	brd.Dev.Init(stm, reg)
}

func (brd *Bluerootdev)Handle(a, b UInt24) (chanX, chanY UInt24, devctl DeviceCtl) {
	/*println(a)
	println(b)*/
	
	if byte(b) == 0x00 { // I'm being addressed.
		var (
			param = b >> 8
		)
		
		switch param {
		case 0x0000: // Break:
			return 0, 0, DeviceCtl_break
		case 0xFFFF: // Shut down:
			return 0, 0, DeviceCtl_shutdown
		}
		
		return 0, 0, DeviceCtl_none
	} else {
		return brd.Dev.Handle(a, b)
	}
}
