Note that related instructions generally differ by just one bit.


FLOW/MEMORY:
0000 soj (set/jif)
0001 jmp
0010 get

0011 otb
0100 psh

0101 wla (wrl/wra)
0110 rdl (note that the lsb is 0, just like how Selector is 0 with wrl)
0111 rda (note that the lsb is 1, just like how Selector is 1 with wra)

LOGIC/ARITHMETIC:
Addition-related logic:
1000 ior
1001 xor
Multiplication-related logic:
1010 and
1011 edt
Arithmetic:
1100 add
1101 sub

1110 mul
1111 div


SPECIAL:
Note that you cannot set a reg specified by chanY (i.e. no `set` (a.k.a. `set0`) ). This makes it easier to determine what changes control flow and what doesn't.
set0  [quit]
set1  [io]
set2  [nrm]
set3  [nrm~]
get1  [pop]
get~1 [pop~]
get2  [swap]
get3  [ent]
get~3 [ext]

Note that all instructions that can change control flow start with [lsb first] x000xxxx. This is one of the reasons soj is the op that the special ops come from, because [io] and [quit] change control flow.

[quit] is #00; this is on purpose. Some errors can result in accidental execution of zeros. It is better to quit on an error like that rather than continue attempting to run. Thus, #00 is not a no-op.
