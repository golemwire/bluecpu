package asm

import (
	"fmt"
	"strconv"
	"io"
	"os"
	
//	"gitlab.com/golemwire/blue" isn't imported, to prevent an import cycle
)

var (
	NameToOpcode = map[string]byte {
		"soj": 0b0000,
		"jmp": 0b0001,
		"get": 0b0010,
		
		"otb": 0b0011,
		"psh": 0b0100,
		
		"wla": 0b0101,
		"rdl": 0b0110,
		"rda": 0b0111,
		
		"ior": 0b1000,
		"xor": 0b1001,
		"and": 0b1010,
		"edt": 0b1011,
		
		"add": 0b1100,
		"sub": 0b1101,
		"mul": 0b1110,
		"div": 0b1111,
	}
	
	// Builtin consts.
	BConsts = map[string]byte {
		// Special instructions:
		
		"quit":  0b0000_0000,
		"io":    0b0100_0000,
		"nrm":   0b1000_0000,
		"nrm~":  0b1100_0000,
		
		"pop":   0b0100_0010,
		"pop~":  0b0101_0010,
		"swap":  0b1000_0010,
		"ent":   0b1100_0010,
		"ext":   0b1101_0010,
		
		// Registers:
		
		"ip": 0,
		"fp": 1,
		"fs": 2,
		"temp": 3,
		
		// Characters:    [TODO: I suggest checking the ASCII table and using the names like ESC and LF and SPACE, etc. This assembly language *does* work in the ASCII standard, after all.]
		
		"\\n": '\n',    // Newline
		"\\e": '\x1B',  // Escape
		"_": ' ',       // Space
	}
)

// Describes a label reference.
type labelRef_t struct {
	addr int
	// Whether this label reference is for an absolute address.
	absolute bool
	// 1, 2, or 3.
	size int
	// False if the label was never referenced.
	used bool
}

// Assembler state; used for recursion when processing the "#include"-style
// assembler directive "%&".
type asmState struct {
	// A map of label names to their addresses.
	labels map[string]int
	// The label references.
	labelRefs map[string][]labelRef_t
}

// TODO: not proofed against bad input
// TODO: doesn't error when the output is > 16MiB
// TODO: consider using "text/scanner" or something in "text/" for this, instead!
//     Instead of reading one whitespace-delimited 'phrase' at a time, streaming
//     in one character at a time would be better, I think.
// The assembler is not well-written yet; it is more of a hack for testing.
// It lets me test the emulator itself pretty well.
func Assemble(input io.Reader, output *[]byte) bool {
	var (
		state = &asmState {
			labels:    make(map[string]int),
			labelRefs: make(map[string][]labelRef_t),
		}
	)
	
	if assemble(input, output, state) { // If successful:
		// Warn about any label references that refer to nonexistent labels:
		for key, refList := range state.labelRefs {
			for _, ref := range refList {
				if ref.used == false {
					fmt.Fprintf(os.Stderr, "Warning: label reference \"%s\" refers to nonexistent label.\n", key)
				}
			}
		}
		return true
	} else { // If unsuccessful:
		return false
	}
}

func assemble(input io.Reader, output *[]byte, state *asmState) bool {
	const (
		invalidPhraseFmt = "Error: \"%s\" is an invalid phrase.\n"
	)
	
	var (
		n int
		err error
		phrase string
	)
	
	for { // Mid-test loop
		n, err = fmt.Fscan(input, &phrase)
		
		if err == io.EOF { break } // LATER: couldn't this cause something to begin to be scanned, and then EOF is encountered, and no error about the junk at the end is produced?
		
		if err != nil { panic(err) }
		
		
		switch phrase[0] {
		case '#': // Hex literal:
			var byteLiteral byte
			n, _ = fmt.Sscanf(phrase[1:], "%2X", &byteLiteral) // This actually works, despite the weird newline rule
			
			if n != 1 {
				fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
				return false
			}
			
			*output = append(*output, byteLiteral) // TODO: using append can result in almost 2x memory use....
			
		case '@':
			if len(phrase) < 3 { // Too short
				fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
				return false
			}
			
			var (
				subPhrase = phrase[2:]
			)
			switch phrase[1] {
			case 'b': // Little-endian binary literal
				var (
					// The indicated data
					data int
					// The amount of bits sequenced
					bitCount int
				)
				
				for bitCount = 0; bitCount < len(subPhrase); bitCount++ {
					switch subPhrase[bitCount] {
					case '0': // Do nothing
					case '1': data |= 1 << bitCount
					default:
						fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
						return false
					}
					
					if bitCount >= 24 {
						fmt.Fprintf(os.Stderr, "Error: \"%s\" is bigger than 3 bytes.\n", phrase)
						return false
					}
				}
				
				// At least one byte:
				*output = append(*output, byte(data))
				if bitCount > 8 { // At least two bytes:
					*output = append(*output, byte(data >> 8))
				}
				if bitCount > 16 { // Three bytes:
					*output = append(*output, byte(data >> 16))
				}
			case '<', '>': // edt control byte
				var (
					edtCtrlByte byte = 0
					shiftDist int
					err error
				)
				
				if len(subPhrase) < 2 {
					fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
					return false
				}
				
				// Get editMode
				if phrase[1] == '<' {
					edtCtrlByte |= 0b00100000 // Insert mode (shift left)
				}
				
				shiftDist, err = strconv.Atoi(string(subPhrase[:2]))
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error: \"%s\" is an invalid edt control.\n", subPhrase)
					return false;
				} else if shiftDist < 0 || shiftDist > 31 { // `shiftDist & 0b00011111 != shiftDist` should work too, but this is clearer.
					fmt.Fprintf(os.Stderr, "Error: %s must be any of 00 through 31.\n", subPhrase[:2])
					return false;
				}
				edtCtrlByte |= byte(shiftDist)
				
				// Find the other two possible flags, '~' and '+':
				for i := 0; i < len(subPhrase[2:]) && i < 2; i++ {
					switch subPhrase[2:][i] {
					case '~': // Complement (invert bits)
						edtCtrlByte |= 0b01000000
					case '+': // Increment
						edtCtrlByte |= 0b10000000
					}
				}
				
				*output = append(*output, byte(edtCtrlByte))
			
			default:
				fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
				return false
			}
		
		case '`': // String literal:
			*output = append(*output, phrase[1:]...)
			
			/*// Get the rest:
			var buf [1]byte
			for { // Mid-test loop
				n, err = input.Read(buf[:])
				if n != 1 { continue }
				if buf[0] == '"' || err == io.EOF { break }
				*output = append(*output, buf[0])
			}*/
		
		case '[': // Builtin const:
			if len(phrase) < 3 || phrase[len(phrase) - 1] != ']' {
				fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
				return false
			}
			name := phrase[1 : len(phrase) - 1]
			bconst, ok := BConsts[name]
			if !ok {
				fmt.Fprintf(os.Stderr, "Error: \"%s\" is not a builtin const.\n", name)
				return false
			}
			*output = append(*output, bconst)
		
		case ':', '=': // Label (reference or declaration)
			var (
				// Whether or not we are dealing with an absolute address
				// ONLY USEFUL when dealing with a label *reference*, not a label *declaration*!
				absolute = phrase[0] == '='
				phrase = phrase[1:]
				// If 0, this label usage is a declaration.
				referenceSize = 0
			)
			
			switch phrase[0] {
			case '1': referenceSize = 1
			case '2': referenceSize = 2
			case '3': referenceSize = 3
			}
			
			if referenceSize == 0 { // If this is a label declaration:
				if len(phrase) < 1 {
					fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
					return false
				} else if absolute {
					fmt.Fprintf(os.Stderr, "Error: label declaration \"%s\" uses '=', which is for label references which are absolute.\n", phrase)
					return false
				}
				
				labelAddr := len(*output)
				
				// Remember this label:
				if _, ok := state.labels[phrase]; ok {
					fmt.Fprintf(os.Stderr, "Error: re-declaration of label \"%s\".\n", phrase)
					return false
				}
				state.labels[phrase] = labelAddr
				
				// If there are any references to this label already, do some filling:
				labelRefList := state.labelRefs[phrase]
				
				for i, ref := range labelRefList {
					labelRefList[i].used = true
					
					// Use a relative address if the label reference says to
					var finalLabel int
					if ref.absolute {
						finalLabel = labelAddr
					} else {
						finalLabel = labelAddr - ref.addr - ref.size
					}
					
					
					if ref.size == 2 && uint(finalLabel) >= 1<<16 {
						fmt.Fprintf(os.Stderr, "Warning: label reference \"%s\" is larger than 2 bytes.\n", phrase)
					} else if ref.size == 1 && uint(finalLabel) >= 1<<8 {
						fmt.Fprintf(os.Stderr, "Warning: label reference \"%s\" is larger than 1 byte.\n", phrase)
					}
					switch ref.size {
					case 3:
						(*output)[ref.addr + 2] = byte(finalLabel >> 16)
						fallthrough
					case 2:
						(*output)[ref.addr + 1] = byte(finalLabel >> 8 )
						fallthrough
					case 1:
						(*output)[ref.addr    ] = byte(finalLabel      )
					}
				}
			} else { // If this is a label reference:
				var phrase = phrase[1:]
				
				if len(phrase) < 2 {
					fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
					return false
				}
				
				label, ok := state.labels[phrase]
				
				if ok { // The label has been declared.
					// Use a relative address if the source code says to
					var finalLabel int
					if absolute {
						finalLabel = label
					} else {
						finalLabel = label - len(*output) - referenceSize
					}
					
					// Write its address:
					switch referenceSize {
					case 1:
						if uint(finalLabel) >= 1<<8 {
							fmt.Fprintf(os.Stderr, "Warning: label reference \"%s\" is larger than 1 byte.\n", phrase)
						}
						*output = append(*output, byte(finalLabel))
					case 2:
						if uint(finalLabel) >= 1<<16 {
							fmt.Fprintf(os.Stderr, "Warning: label reference \"%s\" is larger than 2 bytes.\n", phrase)
						}
						*output = append(*output, byte(finalLabel), byte(finalLabel >> 8))
					case 3:
						*output = append(*output, byte(finalLabel), byte(finalLabel >> 8), byte(finalLabel >> 16))
					}
				} else { // The label hasn't been declared (yet, hopefully):
					// Remember this label reference:
					state.labelRefs[phrase] = append(state.labelRefs[phrase], labelRef_t {
						addr:     len(*output),
						absolute: absolute,
						size:     referenceSize,
					})
					// Make room to write it in *output later:
					switch referenceSize {
					case 1: *output = append(*output, 0)
					case 2: *output = append(*output, 0, 0)
					case 3: *output = append(*output, 0, 0, 0)
					}
				}
			}
		
		case '(': // Code comment!
			var (
				phrase = phrase[1:]
			)
			ended := false
			
			for i := 0; i < len(phrase); i++ {
				if phrase[i] == ')' { // Found the closing parenth...
					if i == len(phrase) - 1 { // ...at the end (good).
						ended = true
					} else { // ...in the middle (bad).
						fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
						return false
					}
				}
			}
			
			if !ended {
				var clParenth [1]byte
				for clParenth[0] != ')' && err != io.EOF {
					n, err = input.Read(clParenth[:])
					if n != 1 { continue }
				}
			}
		
		case '%': // Assemble data from file or insert raw data from file
			if len(phrase) < 3 {
				fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
				return false
			}
			
			
			
			// TODO: no, you shouldn't just open the file. Use a Go
			// stdlib FS, or something.
			switch phrase[1] {
			case '&': // "Gosub" the assembler to a file, assembling from it
				file, err := os.Open(phrase[2:])
				if err != nil { panic(err) }
				
				if func() bool {
					defer file.Close()
					
					return assemble(file, output, state)
				}() == false {
					return false
				}
			case '*': // Embed raw data from a file
				file, err := os.Open(phrase[2:])
				if err != nil { panic(err) }
				data, err := io.ReadAll(file)
				file.Close()
				if err != nil { panic(err) }
				
				*output = append(*output, data...) // TODO: I think that the Golang '...' might put `data` on the stack... could be up to 16MiB in this case, which may be too much.
			default:
				fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
				return false
			}
		default: // Instr literal (maybe):
			var (
				opcode, literal, selector, deref byte
			)
			
			// Assemble the instruction:
			if len(phrase) < 3 {
				fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
				return false
			}
			
			opcode, ok := NameToOpcode[phrase[:3]]
			
			if !ok {
				// Look for wrl, wra, jif, and set:
				switch phrase[:3] {
				case "jif": selector = 1
				case "set": opcode   = NameToOpcode["jos"]
				case "wra": selector = 1
				case "wrl": opcode   = NameToOpcode["wla"]
				default:
					fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
					return false
				}
				
			}
			
			for i := 3; i < len(phrase); i++ { // Parse any flags
				switch phrase[i] { // LATER: issue: accepts the same flag over and over again. Also, accepts any order (I don't know if you want that).
				case '~': selector = 1 // Turn Selector on (use chanY)
				case '=': deref = 1 // Deref bit
				case '0', 'i': // literal is 0. The 0 flag is for style. 'i' means IP reg.
				case '1', 'f': literal = 1 // 'f' = FP reg
				case '2', 's': literal = 2 // 's' = FS reg
				case '3', 't': literal = 3 // 't' = Temp reg
				default:
					fmt.Fprintf(os.Stderr, invalidPhraseFmt, phrase)
					return false
				}
			}
			
			*output = append(*output, opcode | selector << 4 | deref << 5 | literal << 6)
		}
	}
	
	return true
}

func Disassemble(input io.Reader, output *[]byte) {
	var (
		buf [1]byte
		// How many bytes of literal we are currently reading
		litByteCount = 0
	)
	
	for { // Mid-test loop
		var (
			opcode,     selector,     deref,     literal     byte
			opcodeStr,  selectorStr,  derefStr,  literalStr  string
		)
		
		n, err := input.Read(buf[:])
		
		if err == io.EOF { break }
		if n == 0 { continue }
		
		if err != nil { panic(err) }
		
		switch litByteCount {
		case 3: fallthrough
		case 2:
			*output = append(*output, fmt.Sprintf(
				"#%02X ",
				buf[0])...)
			litByteCount--
		case 1:
			*output = append(*output, fmt.Sprintf(
				"#%02X\n",
				buf[0])...)
			litByteCount--
		case 0:
			// Find special instructions:
			switch buf[0] {
			case BConsts["quit"]:
				*output = append(*output, "[quit]\n"...)
				continue
			case BConsts["io"]:
				*output = append(*output, "[io]\n"...)
				continue
			case BConsts["nrm"]:
				*output = append(*output, "[nrm]\n"...)
				continue
			case BConsts["nrm~"]:
				*output = append(*output, "[nrm~]\n"...)
				continue
			case BConsts["pop"]:
				*output = append(*output, "[pop]\n"...)
				continue
			case BConsts["pop~"]:
				*output = append(*output, "[pop~]\n"...)
				continue
			case BConsts["swap"]:
				*output = append(*output, "[swap]\n"...)
				continue
			case BConsts["ent"]:
				*output = append(*output, "[ent]\n"...)
				continue
			case BConsts["ext"]:
				*output = append(*output, "[ext]\n"...)
				continue
			}
			
			// Decode:
			opcode, selector, deref, literal = buf[0] & 0xF,
				                           buf[0] >> 4 & 0b1,
				                           buf[0] >> 5 & 0b1,
				                           buf[0] >> 6
			
			// Find op mnemonic:
			for key, value := range NameToOpcode {
				if opcode == value {
					opcodeStr = key
					break
				}
			}
			
			// Selector bit:
			if selector == 1 {
				selectorStr = "~"
			}
			
			// Deref bit:
			if deref == 1 {
				derefStr = "="
			}
			
			// Literal bits:
			switch literal {
			case 0:
				if deref == 1 {
					literalStr = "0" // For style
				}
			case 1: literalStr = "1"
			case 2: literalStr = "2"
			case 3: literalStr = "3"
			}
			
			if deref == 0 { // If deref is not set, then prepare to load some literal bytes.
				litByteCount = int(literal)
			}
			
			*output = append(*output, fmt.Sprintf(
				"%s%s%s%s\t",
				opcodeStr,
				selectorStr,
				derefStr,
				literalStr)...)
		}
	}
}
















