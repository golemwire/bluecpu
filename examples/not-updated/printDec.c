/*
	This program was to help me understand how to write a binary-to-ASCII-decimal algorithm, so I could write it in assembly.
	This one here isn't the smallest, but it works.
*/

#include <stdio.h>
#include <stdbool.h>

void printDec(unsigned int number);

int main() {
	for (int i = 0; i <= 101; i++) {
		printf("%d\t", i);
		printDec(i);
	}
	
	return 0;
}

void printDec(unsigned int number) {
	char output[9] = {'\0'}; // Since Blue doesn't use zero-terminated strings, we'll not optimize this.
	unsigned int
	     currentPlace;
	int  digitCount = 0;
	bool currentlyPrinting = false;
	
	for (currentPlace = 10000000; currentPlace != 0; currentPlace /= 10) {
		output[digitCount] = '0' + ((number / currentPlace) % 10);
		
		if (output[digitCount] != '0' || currentlyPrinting) { // Skip leading zeros
			currentlyPrinting = true;
			
			digitCount++;
		}
	}
	/*	In the fizzbuzz.basm printDec algorithm:
		if (digitCount == 0) digitCount = 1;
		Or:
		digitCount += digitCount == 0;
		I suppose.
		And then digitCount is used in the console device io operation.
	*/
	
	puts(output);
}
