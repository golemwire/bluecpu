package blue

import (
	"fmt"
	"io"
	"os"
)

type Consoledev struct {
	Stm []byte
}

func (cd *Consoledev)Init(stm []byte, reg *[4]UInt24) {
	cd.Stm = stm
}

func (cd *Consoledev)Handle(a, b UInt24) (chanX, chanY UInt24, devctl DeviceCtl) {
	var (
		cmd  = b & 0xF
		size = b >> 4
	)
	
	switch cmd {
	case 0x0: // Write
		//fmt.Print(string(cd.Stm[a : a + size]))
		if _, err := os.Stdout.Write(cd.Stm[a : a + size]); err != nil { panic (err) }
	case 0x1: // Read
		n, err := os.Stdin.Read(cd.Stm[a : a + size])
		chanX = UInt24(n)
		if err == io.EOF {
			chanY = 1 // Code for EOF
		} else if err != nil {
			panic(err)
		}
	case 0x2: // Write byte
		if _, err := os.Stdout.Write([]byte{byte(a)}); err != nil { panic (err) }
	case 0x3: // Read byte
		// Read until EOF or a byte is read:
		var buf [1]byte
		for { // Mid-test loop (2 tests)
			n, err := os.Stdin.Read(buf[:])
			chanX = UInt24(buf[0])
			
			if err == io.EOF {
				chanY = 1 // Code for EOF
		 break
			} else if err != nil {
				panic(err)
			} else if n == 1 {
		 break
			}
		}
		
	default:
		fmt.Fprintf(os.Stderr, "Consoledev: error: 0x%X is not a command\n", cmd)
		return 0, 0, DeviceCtl_crash
	}
	
	return
}
