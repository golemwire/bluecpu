package blue

// A device, plugged into the CPU.
// It has DMA and register access, and can raise interrupts [TODO].
type Device interface {
	Init(stm []byte, reg *[4]UInt24)
	
	Handle(a, b UInt24) (chanX, chanY UInt24, devctl DeviceCtl)
}

type DeviceCtl byte
const (
	DeviceCtl_none DeviceCtl = 0
	DeviceCtl_break          = 1
	DeviceCtl_shutdown       = 2
	DeviceCtl_crash          = 3
)
