package blue

import (
	"fmt"
	"io"
	"bytes"
	"os"
	"time"
	
	"gitlab.com/golemwire/blue/asm"
)

type CPU struct {
	// The memory pages in use: Exe page, Stack page, Read page, and Write page, respectively.
	XPage, SPage, RPage, WPage []byte
	
	// Attached device (likely a blueroot with a hub):
	Dev Device
	
	// Internal values:
	
	// The channels.
	ChanX, ChanY UInt24
	// The main CPU registers.
	Reg [4]UInt24
	
	// If not nil, send trace data through the Writer.
	Trace io.Writer
}

func NewCPU(device Device, stm []byte) *CPU {
	cpu := &CPU {
		XPage: stm,
		SPage: stm,
		RPage: stm,
		WPage: stm,
		Dev: device,
	}
	//cpu.Reg[Reg_pge] = UInt24(len(stm) - 1).Clipped() // Shouldn't need to be clipped, but might as well
	
	cpu.Dev.Init(stm, &cpu.Reg)
	
	return cpu
}

// Mode settings.
const (
	// Unsigned integer.
	Mode_numberFormat_uInt   = 0b00
	// Signed integer.
	Mode_numberFormat_sInt   = 0b01
	// Unsigned Q16+8 quantity.
	Mode_numberFormat_uQuant = 0b10
	// Signed Q16+8 quantity (Q1+15+8, if you will).
	Mode_numberFormat_sQuant = 0b11
)

// Registers.
const (
	// Instruction Pointer.
	Reg_ip   = 0b00
	// Frame Pointer. Assumed to be initialized to the end of the loaded program image.
	Reg_fp   = 0b01
	// Frame Size.
	Reg_fs   = 0b10
	// General purpose register Temp.
	Reg_temp = 0b11
)

// Normal operations.
const (
	// Set Register or Jump Conditional.
	Op_soj = 0b0000
	// Jump Unconditional.
	Op_jmp = 0b0001 // Judging by your test programs, jumping if nonzero is better than jumping if zero; and, it is more canonical in computer science too, I think.
	// Get Register.
	Op_get = 0b0010
	
	// Output B.
	Op_otb = 0b0011
	// Adjust Mode.
	Op_psh = 0b0100
	
	// Write Local or Write Absolute.
	Op_wla = 0b0101
	// Read Local.
	Op_rdl = 0b0110
	// Read Absolute.
	Op_rda = 0b0111
	
	// Inclusive Or.
	Op_ior = 0b1000
	// Exclusive Or.
	Op_xor = 0b1001
	// And.
	Op_and = 0b1010
	// Edit.
	Op_edt = 0b1011
	
	// Add.
	Op_add = 0b1100
	// Subtract.
	Op_sub = 0b1101
	// Multiply.
	Op_mul = 0b1110
	// Divide.
	Op_div = 0b1111
)

// Special instructions.
const (
	//                             [ LES   Op]
	
	// Quit.
	InstrSpecial_quit  = Op_soj | 0b0000_0000
	// I/O.
	InstrSpecial_io    = Op_soj | 0b0100_0000
	// Normalize X.
	InstrSpecial_nrmX  = Op_soj | 0b1000_0000
	// Normalize Y.
	InstrSpecial_nrmY  = Op_soj | 0b1100_0000
	
	// Pop to X.
	InstrSpecial_popX  = Op_get | 0b0100_0000
	// Pop to Y.
	InstrSpecial_popY  = Op_get | 0b0101_0000
	// Swap Channels.
	InstrSpecial_swap  = Op_get | 0b1000_0000
	// Enter Frame.
	InstrSpecial_ent   = Op_get | 0b1100_0000
	// Exit Frame.
	InstrSpecial_ext   = Op_get | 0b1101_0000
)


// Write 1, 2, or 3 bytes to memory.
// If absolute is true, then absolute; otherwise, local.
// See the Blue architecture documentation on the wla operation.
func (cpu *CPU)Write(data UInt24, addr UInt24, memWordSize int, absolute bool) {
	var (
		page []byte
	)
	
	// Select which page, and handle a memWordSize of 0:
	if absolute {
		page = cpu.WPage
		
		if memWordSize == 0b00 { memWordSize = 1 }
	} else { // Local
		page = cpu.SPage
		
		if memWordSize == 0b00 {
			memWordSize = 3
			// In hardware, you can multiply by 3 quickly with this method: `(inputB << 2) + inputB`.
			// I am not a hardware engineer, so there may be a faster method.
			addr *= 3
		}
		addr += cpu.Reg[Reg_fp] // Note: not clipped.
	}
	
	//fmt.Printf("Writing 0x%X at 0x%X\n", data, addr)
	
	// Write 1, 2, or 3 bytes:
	switch memWordSize {
	case 3:
		page[(addr + 2) & 0xFFFFFF] = byte(data >> 16)
		fallthrough
	case 2:
		page[(addr + 1) & 0xFFFFFF] = byte(data >>  8)
		fallthrough
	case 1:
		page[ addr      & 0xFFFFFF] = byte(data      )
	}
}
// Read 1, 2, or 3 bytes from memory.
// If absolute is true, then absolute; otherwise, local.
// See the Blue architecture documentation on the rdl and rda operations.
func (cpu *CPU)Read(addr UInt24, memWordSize int, absolute bool) (output UInt24) {
	var (
		page []byte
	)
	
	// Select which page, and handle a memWordSize of 0:
	if absolute {
		page = cpu.RPage
		
		if memWordSize == 0b00 { memWordSize = 1 }
	} else { // Local
		page = cpu.SPage
		
		if memWordSize == 0b00 {
			memWordSize = 3
			addr *= 3
		}
		addr += cpu.Reg[Reg_fp] // Note: not clipped.
	}
	
	//fmt.Printf("Reading from 0x%X: ", addr)
	
	// Read 1, 2, or 3 bytes:
	switch memWordSize {
	case 3:
		output =                      UInt24(page[(addr + 2) & 0xFFFFFF]) << 16
		fallthrough
	case 2:
		output = output &^ 0x00FF00 | UInt24(page[(addr + 1) & 0xFFFFFF]) << 8
		fallthrough
	case 1:
		output = output &^ 0x0000FF | UInt24(page[ addr      & 0xFFFFFF])
	}
	
	//fmt.Printf("read 0x%X.\n", output)
	
	return
}


// Runs the CPU.
// Returns when the CPU is halted.
// Returns the cycle count.
func (cpu *CPU)Run() uint64 {
	defer func() {
		if r := recover(); r != nil {
			fmt.Fprintf(os.Stderr, "\nError while emulating: %v\n", r) // The newline before is because we aren't sure what (perhaps garbage) text there is before this
			fmt.Fprintf(os.Stderr, "ip: %d (0x%06X)\n", cpu.Reg[Reg_ip], cpu.Reg[Reg_ip])
			panic(r)
		}
	}()
	
	var (
		totalCycles uint64 = 0
	)
	
	for { // Post-test loop
		continueRunning := cpu.Cycle()
		totalCycles++
		if !continueRunning { break }
	}
	
	return totalCycles
}


// Returns true if the CPU isn't yet halted.
func (cpu *CPU)Cycle() bool {
	// Trace, if enabled
	var ipBeforeFetch, ipAfterFetch UInt24
	if cpu.Trace != nil {
		ipBeforeFetch = cpu.Reg[Reg_ip]
		ipAfterFetch  = ipBeforeFetch + 1
		
		//fmt.Fprintf(cpu.Trace, "%X@\t", cpu.Reg[Reg_ip])
		defer func() {
			debugText := make([]byte, 16)[:0]
			asm.Disassemble(bytes.NewReader(cpu.XPage[ipBeforeFetch:ipAfterFetch]), &debugText)
			fmt.Fprint(cpu.Trace, string(debugText))
			time.Sleep(time.Second / 10)
		}()
	}
	
	// Fetch:
	instr := cpu.XPage[cpu.Reg[Reg_ip]]
	cpu.Reg[Reg_ip] = (cpu.Reg[Reg_ip] + 1) & 0xFFFFFF // Increment ip reg
	
	// Find special instructions:
	// Special instructions can access the device, the channels, memory,
	// and the registers.
	switch instr {
	case InstrSpecial_quit:
		cpu.ChanY = 0xFFFF00
		fallthrough
	case InstrSpecial_io:
		switch chanX, chanY, devctl := cpu.Dev.Handle(cpu.ChanX, cpu.ChanY); devctl {
		case DeviceCtl_shutdown:
			return false
		case DeviceCtl_crash:
			panic("Error: crashed via devicectl")
		default:
			cpu.ChanX, cpu.ChanY = chanX, chanY
			return true
		}
	case InstrSpecial_nrmX:
		if cpu.ChanX != 0 { cpu.ChanX = 1 }
		return true
	case InstrSpecial_nrmY:
		if cpu.ChanY != 0 { cpu.ChanY = 1 }
		return true
	
	case InstrSpecial_popX:
		cpu.Reg[Reg_fs] = (cpu.Reg[Reg_fs] - 3) & 0xFFFFFF
		cpu.ChanX       = cpu.Read(cpu.Reg[Reg_fs], 3, false)
		return true
	case InstrSpecial_popY:
		cpu.Reg[Reg_fs] = (cpu.Reg[Reg_fs] - 3) & 0xFFFFFF
		cpu.ChanY       = cpu.Read(cpu.Reg[Reg_fs], 3, false)
		return true
	case InstrSpecial_swap:
		cpu.ChanX, cpu.ChanY = cpu.ChanY, cpu.ChanX
		return true
	case InstrSpecial_ent:
		cpu.Reg[Reg_fp] = (cpu.Reg[Reg_fp] + cpu.Reg[Reg_fs] - cpu.ChanY) & 0xFFFFFF
		cpu.Reg[Reg_fs] = cpu.ChanY
		return true
	case InstrSpecial_ext:
		cpu.Reg[Reg_fs] =  cpu.Read(0xFFFFFF /*-1*/, 0b00, false)
		cpu.Reg[Reg_fp] =  (cpu.Reg[Reg_fp] - (cpu.Reg[Reg_fs] + 3)) & 0xFFFFFF
		return true
	}
	
	// Decode:
	opcode, selector, embedded, literal := instr & 0xF,
	                                       instr >> 4 & 0b1 == 1,
	                                       instr >> 5 & 0b1 == 1,
	                                       instr >> 6
	
	// Pre-execution:
	var (
		inputA = cpu.ChanX
		inputB = cpu.ChanY
		output UInt24
		noOutput bool
	)
	// Get literal:
	if embedded { // The field "literal" as the literal
		inputB = UInt24(literal)
	} else { // The bytes after this instr as the literal
		switch literal {
		/* [If 0, then inputB doesn't get overwritten.] */
		case 1: inputB = UInt24(cpu.XPage[cpu.Reg[Reg_ip]])
		case 2: inputB = UInt24(cpu.XPage[cpu.Reg[Reg_ip]]) | UInt24(cpu.XPage[cpu.Reg[Reg_ip] + 1]) << 8
		case 3: inputB = UInt24(cpu.XPage[cpu.Reg[Reg_ip]]) | UInt24(cpu.XPage[cpu.Reg[Reg_ip] + 1]) << 8 | UInt24(cpu.XPage[cpu.Reg[Reg_ip] + 2]) << 16
		}
		
		cpu.Reg[Reg_ip] = (cpu.Reg[Reg_ip] + UInt24(literal)) & 0xFFFFFF // Skip ip reg over the literal (if any)
	}
	
	// For tracing
	ipAfterFetch = cpu.Reg[Reg_ip]
	
	// Execute:
	output, noOutput = cpu.Exec(opcode, selector, inputA, inputB)
	
	// Post-execution:
	if !noOutput {
		if selector {
			cpu.ChanY = output
		} else {
			cpu.ChanX = output
		}
	}
	
	return true
}

// Execute a decoded normal/non-special instruction.
// The ip reg should point to the following instr, not the current one.
// The only fields of cpu that are accessed are .Reg and .{X,S,R,W}Page. For .Reg, at most one register is gotten and one is set (if one is set it is the same register) (not including the Mode register), and for .{X,S,R,W}Page, at most one memory read or write occurs. (Remember, this is supposed to be theoretically hardware-implementable in a relatively simple way.)
func (cpu *CPU)Exec(opcode byte, selector bool, inputA, inputB UInt24) (output UInt24, noOutput bool) {
	switch opcode {
	case Op_soj:
		if selector { // jif
			if inputA != 0 {
				cpu.Reg[Reg_ip] = (cpu.Reg[Reg_ip] + inputB) & 0xFFFFFF
			}
		} else {      // set
			cpu.Reg[inputB & 0b11] = inputA
		}
		noOutput = true
	case Op_jmp:
		output = cpu.Reg[Reg_ip]
		cpu.Reg[Reg_ip] = (cpu.Reg[Reg_ip] + inputB) & 0xFFFFFF
		
		noOutput = selector
	case Op_get:
		output = cpu.Reg[inputB & 0b11]
	
	case Op_otb:
		output = inputB
	case Op_psh:
		if selector {
			cpu.Write(inputB, cpu.Reg[Reg_fs], 3, false)
		} else {
			cpu.Write(inputA, cpu.Reg[Reg_fs], 3, false)
		}
		cpu.Reg[Reg_fs] = (cpu.Reg[Reg_fs] + 3) & 0xFFFFFF
		
		noOutput = true
	
	case Op_wla:
		cpu.Write(inputA, inputB, 0, selector)
		
		noOutput = true
	case Op_rdl:
		output = cpu.Read(inputB, 0, false)
	case Op_rda:
		output = cpu.Read(inputB, 0, true)
	
	case Op_ior:
		output = inputA | inputB
	case Op_xor:
		output = inputA ^ inputB
	case Op_and:
		output = inputA & inputB
	case Op_edt:
		var (
			shiftDist  = inputB & 0b11111
			editMode   = inputB >> 5 & 0b1
			complement = inputB >> 6 & 0b1
			increment  = inputB >> 7 & 0b1
			mask       = inputB >> 8
		)
		if editMode == 1 { // Insert mode
			output = inputA << shiftDist & 0xFFFFFF |  mask
		} else {           // Delete mode
			output = inputA >> shiftDist            &^ mask
		}
		output ^= 0xFFFFFF + (complement ^ 0b1) // Not clipped
		output += increment
		output &= 0xFFFFFF
	
	case Op_sub:
		output = (inputA - inputB) & 0xFFFFFF
	case Op_add:
		output = (inputA + inputB) & 0xFFFFFF
	case Op_mul:
		output = (inputA * inputB) & 0xFFFFFF
	case Op_div:
		if inputB == 0 { // Handle divide by 0
			output = 0xFFFFFF
		} else {
			output = inputA / inputB
		}
	}
	
	return
}
