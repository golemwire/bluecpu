A var_spec:
	identifier type_spec
A type_spec:
	type_name
		Can be word, pair, or byte.
	number type_spec
		E.g. `8 word` for an array of 8 words
	(var_spec, var_spec, ...)
		A struct definition! E.g. (p_start *[TYPE], len word)


[[Definitions comment]]
	[const_name]	meaning
	[const_name generic_const]	meaning_that_can_use_[generic_const]
		continued_meaning,_indented
[[Arrays]]
	[array TYPE]	(addr [TYPE], len word)
	[slice TYPE]	(addr [TYPE], len word, cap word)
[[Math]]
	; Modulo. Warning: Uses reg A.
	[%]	swb-*2	div	mul-	get*2	sub
	
	; Q16+8 fixed-point divide. The dividend will lose 4 bits of precision, and the divisor needs 4 bits of "headroom" on the high end (to prevent overflow).
	; Inaccurate and slow; it is best to try to divide by an integer, if you can.
	; TODO: test this... does it work. And, you might be able to OR in a bit into y to have rounding!
	; Formula: x = (x << 4) / (y >> 4)
	[qdv]	[swap]	[-qdv]
	; [qdv], except the inputs are swapped
	[-qdv]	edt1	#24	[swap]	edt1	#04	div
	
	; [qdv], except the dividend needs 8 bits of headroom and the divisor is unchanged.
	; Formula: x = (x << 8) / y
	; This is a reason to use Q16+8 instead of Q12+12: we can just shift the dividend the size of the fractional part, and have space left over. (It also means the intermediary value in qml is cleanly 4 bytes.)
	[qdv_precise]	edt1	#08	div		[TODO NEXT: finish qdv_precise, and use it in your qdiv-test.basm test]
[[Math constants]]
	; The golden ratio
	[phi]	0x19E	(1.6171875)
[[Fast math]]
	[x3]	edt-*1	add
	; Adding '-' to the end of the name of a constant will clear the select bit of the constant, if that name is unused; e.g. [x3-] results in `edt-*1 add-`, unless a constant named [x3-] is defined, in which case that will be used instead. Adding '-' to the beginning does the same except with prepending a [swap].
[[Lvalue to pointer]]
	[lt*]	get-*1	sub
[[Dereferencing]]
	[*b]	[lt*-]	rdb
	[*p]	[lt*-]	rdp
	[*w]	[lt*-]	rdw
[[Basic I/O]]
	; x is the addr, y is the len.
	[prnt]	[swap]	[-prnt]
	[-prnt]	and2	#FF #0F	xor3	#00 #00 #01	; Turn len into a Consoledev cmd
		[swap]	[io]





//Scope comment//
var_spec
var_spec
...
	Body
 ^?	Continue condition
 v?	Break condition
 ^^	Continue unconditional
 vv	Break unconditional





{{Function comment}}
{identifier} type_spec type_name
	Body. The identifier is the function name. The type_spec is the parameter, and must be a struct definition. The second is the return type.
 ^?	Continue condition
 v?	Break condition

{{Function comment}}
{identifier generic_const} type_spec type_name
	Code that can use [generic_const]. One version of the function is built for each use per generic_const (like the way you think of generics).

Functions can be part of a scope body!






<<Switch comment>>
Condition (Note that if you omit this, then this switch will act like a 'switch {...}' ('switch true') in Go. To use chanX as the condition, use the placeholder "-".)
 <Case1> ; Code that outputs a value, such as "123"
	Body
 vv
 <Case2>
	Body
 ^	Continue condition
 <>	
 	Default case body




:identifier: create an identifier that refers to a byte index. (This is a label.)
=3identifier: the absolute pointer of identifier (can be a variable or a function).
.3identifier: the lvalue (stack-relative pointer) of identifier.
:3identifier: you know, same as Blue assembly.
&identifier: shorthand for `otb3 =3identifier`. `&identifier-` is here too.
Because of this, and maybe other things, I don't think stack frames should be allowed (at the language level) to be of variable size (you don't have a way to specify it in this language either).
To dereference, use one of the [*w], [*p-], etc. stdlib consts.
identifier.identifier: access something in a struct.
[const_name any_generic_consts]: embed a const
"string":          embedded data (\" may be used for a literal ")
0x20               embedded hex.
0d32:              embedded decimal.
0b00010000         embedded binary.
32:                output embedded data (one byte in this case) (shortcut for `otb1 #20`). Also 32-.
#20:               same, but hex.
identifier: get the value of a variable.
identifier-: same, but outputs to chanY
+identifier: set a variable.
\CHAR: escape a character. E.g. if you want to provide a generic const with a space: [const_name generic\ const]
%PATH: #include another file.
/Literal/: embeds data, BUT jumps over it safely, and outputs its absolute address on chanY. (Think things like 'struct literals' in Go!) ChanX keeps its value. The way this works is really cool: /0x7331/ compiles to `jmp-1 :1theLiteral 0x7331 :theLiteral` (`jmp-1 0x02 0x31 0x73`). Note that this (1) outputs the absolute address of where the data is stored! (2) does so accurately regardless of where the code is loaded (position-independant code (think PIE) ), and (3) embeds the data in-line where it is entered in the source, keeping Bluelle literal. It also embeds the length of the data (potentially useful for decompiling).
(Literal): Same but without touching any chans.


	 ^^						Code
	Where this is aligned matters.                  As well as the alignment of this.
        Its alignment says where to continue.           It says at which level the code gets executed.
                                                        Use the - no-op if no code needs to go here.



Remember that this is a low-level language. Just think of it as an extension of the Blue assembly language which allows for better handling of scope (scope "lifecycle" and local variable getting and setting), flow (break, continue, switch, call), consts/macros, and data structures.
Also... BoB!!




Compiler implementation idea:
	Have functions for each key-char/keyword. E.g. parseFunc for "{{", parseFuncCall for "{", etc. It is like recursion. The functions read the source code byte by byte, and when they reach the end of their data (e.g. "]" in the case of parseConstInstance) they return. If they encounter a key-char/keyword, they'll call the relevant function (thus nesting deeper recursion-style). Instead of recursion-style for indentation, perhaps you should have global data or something (you need a way to understand the continues and breaks.
	All of these functions are appending to a buffer of the compiled output.
	Note that this design means that you should have key-chars/keywords at the beginning of something. Getting variable values and embedding variables are exceptions.
	Require tabs, not spaces. It'll make it much, much easier to count indentation. Ignore a single space after a tab, though.
