package blue

import (
	"fmt"
	"os"
)

// Hub device. The low byte of b is the device address, and is used to select which device.
// Device address 0x00 shouldn't be used, as this hub is probably plugged into a Bluerootdev.
// Don't initialize the sub-devices manually, as Hubdev does it when it is initialized.
type Hubdev struct {
	// The length is not 256, because of Blueroot being 0x00.
	// Subtraction method hash (subtract 1)
	Devices [255]Device
}

func (hd *Hubdev)Init(stm []byte, reg *[4]UInt24) {
	for _, device := range hd.Devices {
		if device == nil { continue }
		device.Init(stm, reg)
	}
}

func (hd *Hubdev)Handle(a, b UInt24) (chanX, chanY UInt24, devctl DeviceCtl) {
	devAddr := byte(b)
	
	if hd.Devices[devAddr - 1] == nil {
		fmt.Fprintf(os.Stderr, "Hubdev error: device address %02X has no device.\n", devAddr)
		return 0, 0, DeviceCtl_crash
	}
	
	return hd.Devices[devAddr - 1].Handle(a, b >> 8)
}
