package main

import (
	"os"
	"os/signal"
	"syscall"
	"fmt"
	"io"
	
	"gitlab.com/golemwire/blue"
)

var (
	// 16MiB of RAM.
	stm [1<<24]byte
	
	traceOutput io.Writer = nil
)

func main() {
	var (
		err error
		n int
		imageFile *os.File
		imageSize int
	)
	
	switch len(os.Args) {
	case 3:
		if os.Args[2] == "--trace" {
			traceOutput = os.Stderr
		} else {
			fmt.Fprintf(os.Stderr, "Problem: “%s” is not an option.\n", os.Args[2])
			os.Exit(1)
		}
		fallthrough
	case 2:
		imageFile, err = os.Open(os.Args[1])
		if err != nil { panic(err) }
	default:
		fmt.Fprintf(os.Stderr, "Problem: wrong amount of arguments.\n")
		os.Exit(1)
	}
	
	n, err = 0, nil
	for imageSize = 0; err != io.EOF; imageSize += n {
		n, err = imageFile.Read(stm[imageSize:])
		if err != nil && err != io.EOF { panic(err) }
	}
	
	
	var (
		totalCycles uint64
		running = true
		signals = make(chan os.Signal, 1)
	)
	
	cpu := blue.NewCPU(&blue.Bluerootdev {
		Dev: &blue.Hubdev {
			Devices: [255]blue.Device {
				&blue.Consoledev{},
			},
		},
	}, stm[:])
	// Init frame pointer
	cpu.Reg[blue.Reg_fp] = blue.UInt24(imageSize & 0xFFFFFF)
	
	// Enable tracing the running instructions, if enabled
	cpu.Trace = traceOutput
	// Catch SIGUSR1 and SIGINT (usually Ctrl+C)
	signal.Notify(signals, syscall.SIGUSR1, syscall.SIGINT)
	
	defer regDump(cpu) // cpu must not point elsewhere by the time this call happens.
	
	for running {
		select {
		case s := <-signals:
			switch s {
			case syscall.SIGUSR1:
				fmt.Fprint(os.Stderr, "\n\n")
				regDump(cpu)
				fmt.Fprintln(os.Stderr)
			case syscall.SIGINT:
				running = false
			}
		default:
			running = cpu.Cycle()
			totalCycles++
		}
	}
	
	fmt.Fprintf(os.Stderr, "\nCPU halted. %d cycles total.\n", totalCycles)
}

func regDump(cpu *blue.CPU) {
	// Dump channels:
	fmt.Fprintf(os.Stderr, "X:    0x%06X  |  Y:    0x%06X\n",
		cpu.ChanX, cpu.ChanY)
	// Dump registers:
	fmt.Fprintf(os.Stderr, "IP:   0x%06X  |  FP:   0x%06X\n" + 
		               "FS:   0x%06X  |  Temp: 0x%06X\n",
		cpu.Reg[blue.Reg_ip], cpu.Reg[blue.Reg_fp],
		cpu.Reg[blue.Reg_fs], cpu.Reg[blue.Reg_temp])
	// Dump the first four words of the stack frame:
	fmt.Fprint (os.Stderr, "Low stack frame:")
	for i := 0; i < 12; i++ {
		fmt.Fprintf(os.Stderr, " %02X", cpu.SPage[int(cpu.Reg[blue.Reg_fp]) + i])
	}
	fmt.Fprintln(os.Stderr)
}
